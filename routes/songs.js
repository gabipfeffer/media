const express = require('express');
const debug = require('debug')('media:songs-router');


const songsController = require('../controllers/songs');

const router = express.Router();

// get all songs
router.get('/', songsController.getAll);

// get song by id
router.get('/:id', songsController.getById);

// create new song
router.post('/create', songsController.createSong);

// edit song
router.put('/:id', songsController.updateSong);

// delete song
router.delete('/:id', songsController.deleteSong);


module.exports = router;
