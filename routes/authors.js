const express = require('express');

const authorsController = require('../controllers/authors');

const router = express.Router();

// get all authors
router.get('/', authorsController.getAll) 

// get author by id
router.get('/:id', authorsController.getById) 

// create new author
router.post('/create', authorsController.createAuthor) 

module.exports = router;