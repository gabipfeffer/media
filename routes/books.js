const express = require('express');
const debug = require('debug')('media:books-router');

const booksController = require('../controllers/books');

const router = express.Router();

// get all books
router.get('/', booksController.getAll);

// get book by id
router.get('/:id', booksController.getById);

// create new book
router.post('/create', booksController.createBook);

module.exports = router;
