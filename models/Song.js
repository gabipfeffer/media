const mongoose = require('mongoose');

const { Schema } = mongoose;

const songSchema = new Schema(
  {
    title: { type: String, required: true, trim: true, lowercase: true },
    authorId: { type: Schema.Types.ObjectId, ref: 'Author' },
    // @TODO Change the model to support future years.
    year: { type: Number, min: 0 },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = doc._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const Song = mongoose.model('Song', songSchema);

module.exports = Song;
