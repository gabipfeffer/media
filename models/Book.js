const mongoose = require('mongoose');

const { Schema } = mongoose;

const bookSchema = new Schema(
  {
    title: { type: String, required: true, minlength: '2', trim: true, lowercase: true },
    year: { type: Number },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = doc._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      },
    },
  }
);

const Book = mongoose.model('Book', bookSchema);

module.exports = Book;
